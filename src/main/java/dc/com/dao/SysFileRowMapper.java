package dc.com.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import dc.com.entity.SysFile;

public class SysFileRowMapper implements RowMapper<SysFile> {

	@Override
	public SysFile mapRow(ResultSet rs, int row) throws SQLException {
		SysFile sf = new SysFile();
		sf.setKey(rs.getString("KEY"));
		sf.setType(rs.getString("TYPE"));
        sf.setName(rs.getString("NAME"));
        sf.setExt(rs.getString("EXT"));
        sf.setBytes(rs.getLong("BYTES"));
        sf.setDataPath(rs.getString("DATA_PATH"));
        sf.setDataGroup(rs.getString("DATA_GROUP"));
        sf.setExpired(rs.getDate("EXPIRED"));
        sf.setDescInfo(rs.getString("DESC_INFO"));
        sf.setUpdateBy(rs.getString("UPDATE_BY"));
        sf.setUpdateTime(rs.getDate("UPDATE_TIME"));
		return sf;
	}

}
