package dc.com.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSONObject;

@Repository
public class MstCodeComDao extends BaseJdbcDao{
	
	private static final String SQL_SELECT_CODE_LIST = "SELECT CODE,NAME,ATTRO1,ATTRO2,ATTRO3,ATTRO4,"
			+ "ATTRO5,ATTRO6,ATTRO7,ATTRO8,ATTRO9,ATTRO10,EXPIRED FROM MST_CODE WHERE TYPE= ? ";
	
	public List<JSONObject> getTypeList(String type) {
		return this.queryForJsonList(SQL_SELECT_CODE_LIST, type);
	}
}
