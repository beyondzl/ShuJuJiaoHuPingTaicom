package dc.com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;

import dc.com.dao.MstCodeComDao;
import dc.com.util.FastJsonConvert;
import redis.clients.jedis.JedisCluster;
@Service
public class MstCodeComService {
	 /** 缓存标识前缀 */
    private static final String CACHE_ID = "MST_CODE";
	
	@Autowired
	private MstCodeComDao mstCodeComDao;
	
	@Autowired
	private JedisCluster jedisCluster;
	
	public MstCodeComDao getMstCodeComDao(){
		return mstCodeComDao;
	} 
	@Autowired
	public void setMstCodeComDao(MstCodeComDao mstCodeComDao) {
		this.mstCodeComDao = mstCodeComDao;
	}
	
	public List<JSONObject> getTypeList(String type) {
		String ret = jedisCluster.hget(CACHE_ID, type);
		if(ret == null) {
			System.out.println("*********查询数据库**************");
			List<JSONObject> list = this.mstCodeComDao.getTypeList(type);
			jedisCluster.hset(CACHE_ID, type, list.toString());
			ret = list.toString();
		}
		return FastJsonConvert.convertJSONToArray(ret, JSONObject.class);
	}
}
